import 'package:btapp/show_more_button.dart';
import 'package:btapp/view_detail/app_bar2.dart';
import 'package:btapp/view_detail/three_button.dart';
import 'package:flutter/material.dart';

class Sesson extends StatefulWidget {
  Sesson({Key key}) : super(key: key);
  @override
  _SessonState createState() => _SessonState();
}

class _SessonState extends State<Sesson> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: ListView.builder(
        itemCount: 1,
        itemBuilder: (context, i) => Column(
          children: [
            AppBar2Screen(),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 24),
              child: Column(
                children: [
                  Padding(
                    padding: const EdgeInsets.only(top: 25, bottom: 12),
                    child: Row(
                      children: [
                        Text(
                          '12 Nov 2020, 7:30pm',
                          style: TextStyle(
                            color: Colors.red,
                            fontSize: 20,
                            fontWeight: FontWeight.w500,
                          ),
                        ),
                      ],
                    ),
                  ),
                  Column(
                    children: [
                      Padding(
                        padding: const EdgeInsets.only(bottom: 12.0),
                        child: Row(
                          children: [
                            Text(
                              'Zumba Session',
                              style: TextStyle(
                                  fontWeight: FontWeight.w700, fontSize: 28),
                            ),
                          ],
                        ),
                      ),
                      Row(
                        children: [
                          Text(
                            'Lansdowne Hotel, Chippendale',
                            style: TextStyle(
                                fontWeight: FontWeight.w200,
                                fontSize: 16,
                                color: Color(0xff0e0e0e)),
                          ),
                        ],
                      ),
                    ],
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 25.0, bottom: 23),
                    child: ThreeButtonScreen(),
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: [
                      Image.asset('images/ba.png'),
                      RichText(
                        text: TextSpan(
                            style: TextStyle(wordSpacing: 1, fontSize: 14),
                            children: [
                              TextSpan(
                                text: '42 people are going including',
                                style: TextStyle(color: Colors.black),
                              ),
                              TextSpan(
                                  text: ' 2 friends',
                                  style: TextStyle(color: Colors.blue))
                            ]),
                      ),
                    ],
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(vertical: 19.0),
                    child: Divider(
                      thickness: 1,
                      color: Colors.grey,
                    ),
                  ),
                  ShowMoreScreen(),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      TextButton(
                        child: Row(
                          children: [
                            Container(
                              padding: EdgeInsets.only(right: 10),
                              child: Image.asset(
                                'images/king.png',
                                height: 36,
                                width: 36,
                              ),
                            ),
                            Text(
                              'Kings of Trivia',
                              style: TextStyle(
                                  fontSize: 18,
                                  fontWeight: FontWeight.w700,
                                  color: Colors.black),
                            ),
                          ],
                        ),
                        onPressed: () {},
                      ),
                      OutlinedButton(
                        style: ButtonStyle(
                          backgroundColor:
                              MaterialStateProperty.all(Colors.white),
                          shape: MaterialStateProperty.all(
                              RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(5))),
                        ),
                        child: Row(
                          children: [
                            Image.asset(
                              'images/Add.png',
                              width: 40,
                              height: 40,
                            ),
                            Text('Follow')
                          ],
                        ),
                        onPressed: () {},
                      ),
                    ],
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 33.0, bottom: 24),
                    child: Divider(
                      thickness: 1,
                      color: Colors.grey,
                    ),
                  ),
                  Row(
                    children: [
                      Text('Friends attending'),
                    ],
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 16.0, bottom: 176),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Image.asset(
                          'images/gai.png',
                          height: 50,
                          width: 50,
                        ),
                        OutlinedButton(
                          style: ButtonStyle(
                            backgroundColor:
                                MaterialStateProperty.all(Colors.white),
                            shape: MaterialStateProperty.all(
                                RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(5))),
                          ),
                          child: Row(
                            children: [
                              Image.asset(
                                'images/Add.png',
                                width: 60,
                                height: 32,
                              ),
                              Text(
                                'Invite Friends',
                                style: TextStyle(fontSize: 18),
                              )
                            ],
                          ),
                          onPressed: () {},
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
            SizedBox(
              width: MediaQuery.of(context).size.width,
              height: 64,
              child: OutlinedButton(
                style: ButtonStyle(
                  backgroundColor: MaterialStateProperty.all(Colors.blue),
                  shape: MaterialStateProperty.all(RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(5))),
                ),
                child: Text(
                  'RSVP',
                  style: TextStyle(fontSize: 18, color: Colors.white),
                ),
                onPressed: () {},
              ),
            ),
          ],
        ),
      ),
    );
  }
}
