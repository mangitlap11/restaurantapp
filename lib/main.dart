import 'package:btapp/view_detail/demo_rxdart.dart';
import 'package:flutter/material.dart';
import 'push.dart';
import 'model.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key}) : super(key: key);

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  DemoRxDartBloC _demoRxDartBloC = DemoRxDartBloC();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xfff4f5f8),
      appBar: AppBar(
          elevation: 0,
          backgroundColor: Colors.white,
          leading: Image.asset('images/menu.png', color: Colors.blue),
          title: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(
                "What's On",
                style: TextStyle(color: Colors.black),
              ),
              Image.asset(
                'images/book.png',
                color: Colors.blue,
              ),
            ],
          )),
      body: StreamBuilder<List<Event>>(
          stream: _demoRxDartBloC.listevent,
          builder: (context, snapshot) {
            if (snapshot.data == null) {
              print(_demoRxDartBloC.event[0].nameEvent);
              return Text('loading');
            } else
              return ListView.builder(
                itemCount: snapshot.data.length,
                itemBuilder: (context, i) => Padding(
                  padding: const EdgeInsets.all(20.0),
                  child: Container(
                    width: 327,
                    height: 180,
                    child: TextButton(
                      onPressed: () {
                        Navigator.push(context,
                            MaterialPageRoute(builder: (context) => Sesson()));
                      },
                      child: Card(
                        elevation: 0,
                        semanticContainer: true,
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(10)),
                        child: Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 23),
                          child: Column(children: [
                            Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Text(snapshot.data[i].nameEvent,
                                          style: TextStyle(
                                              fontSize: 24,
                                              fontWeight: FontWeight.bold)),
                                      Text(snapshot.data[i].locationName,
                                          style: TextStyle(
                                            fontSize: 16,
                                            fontWeight: FontWeight.w300,
                                          ))
                                    ],
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.only(right: 10.0),
                                    child: TextButton(
                                      child: Container(
                                        height: 50,
                                        width: 50,
                                        child: Icon(
                                          Icons.favorite,
                                          color: snapshot.data[i].isLike
                                              ? Colors.blue
                                              : Colors.white,
                                        ),
                                        decoration: BoxDecoration(
                                            borderRadius:
                                                BorderRadius.circular(50),
                                            border: Border.all(
                                                color: Colors.blue, width: 2),
                                            color: snapshot.data[i].isLike
                                                ? Colors.white
                                                : Colors.blue),
                                      ),
                                      onPressed: () {
                                        _demoRxDartBloC.setLike(i);
                                      },
                                    ),
                                  ),
                                ]),
                            Divider(
                              thickness: 1,
                              color: Colors.grey,
                            ),
                            Padding(
                              padding: const EdgeInsets.only(left: 15.0),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: [
                                  Image.asset(
                                    snapshot.data[1].oganiser.imagePath,
                                    width: 20,
                                    height: 20,
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.only(left: 30.0),
                                    child: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        Padding(
                                          padding: const EdgeInsets.only(
                                              bottom: 6.0),
                                          child: Text(
                                              snapshot.data[1].oganiser.name,
                                              style: TextStyle(
                                                  fontWeight: FontWeight.bold,
                                                  fontSize: 18)),
                                        ),
                                        Text(
                                          snapshot.data[1].oganiser.email,
                                          style: TextStyle(fontSize: 14),
                                        ),
                                      ],
                                    ),
                                  )
                                ],
                              ),
                            )
                          ]),
                        ),
                      ),
                    ),
                  ),
                ),
              );
          }),
    );
  }
}
