import 'package:btapp/view_detail/demo_rxdart.dart';
import 'package:flutter/material.dart';
import 'demo_rxdart.dart';

class ThreeButtonScreen extends StatefulWidget {
  @override
  _ThreeButtonScreenState createState() => _ThreeButtonScreenState();
}

class _ThreeButtonScreenState extends State<ThreeButtonScreen> {
  DemoRxDartBloC _demoRxDartBloC = DemoRxDartBloC();

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceAround,
      children: [
        StreamBuilder<bool>(
            stream: _demoRxDartBloC.isGoingButton,
            initialData: false,
            builder: (context, snapshot) {
              print(snapshot.data);
              return OutlinedButton(
                style: ButtonStyle(
                  backgroundColor: snapshot.data
                      ? MaterialStateProperty.all(Color(0xfff4f5f8))
                      : MaterialStateProperty.all(Colors.blue),
                  shape: MaterialStateProperty.all(RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(5))),
                ),
                child: Row(
                  children: [
                    Image.asset(
                      'images/enter.png',
                      width: 50,
                      height: 60,
                    ),
                    Text(
                      "I'm going !",
                      style: TextStyle(fontSize: 22, color: Colors.black),
                    )
                  ],
                ),
                onPressed: () {
                  _demoRxDartBloC.setGoingButton();
                },
              );
            }),
        StreamBuilder<Object>(
            stream: _demoRxDartBloC.isFavoriteButton,
            initialData: false,
            builder: (context, snapshot) {
              return OutlinedButton(
                style: ButtonStyle(
                  backgroundColor: snapshot.data
                      ? MaterialStateProperty.all(Color(0xfff4f5f8))
                      : MaterialStateProperty.all(Colors.blue),
                  shape: MaterialStateProperty.all(RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(5))),
                ),
                child: Image.asset(
                  'images/heart.png',
                  width: 30,
                  height: 60,
                ),
                onPressed: () {
                  _demoRxDartBloC.setFavoriteButton();
                },
              );
            }),
        StreamBuilder<bool>(
            stream: _demoRxDartBloC.isShareButton,
            initialData: false,
            builder: (context, snapshot) {
              return OutlinedButton(
                style: ButtonStyle(
                  backgroundColor: snapshot.data
                      ? MaterialStateProperty.all(Color(0xfff4f5f8))
                      : MaterialStateProperty.all(Colors.blue),
                  shape: MaterialStateProperty.all(RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(5))),
                ),
                child: Image.asset(
                  'images/enter.png',
                  width: 30,
                  height: 60,
                ),
                onPressed: () {
                  _demoRxDartBloC.setShareButton();
                },
              );
            }),
      ],
    );
  }
}
