import 'package:rxdart/rxdart.dart';
import 'package:btapp/model.dart';

class DemoRxDartBloC {
  List<Event> event = [
    Event('MANCLASS', 'TP HUE', User('MAN', 'images/man.jpg', 'man@gmail.com'),
        false),
    Event('HUYCLASS', 'TP HUE', User('HUY', 'images/huy.jpg', 'huy@gmail.com'),
        false),
    Event('HUNGCLASS', 'TP HUE',
        User('HUNG', 'images/hung.jpg', 'hung@gmail.com'), false),
    Event('ANCLASS', 'TP HUE', User('AN', 'images/an.jpg', 'an@gmail.com'),
        false),
    Event('TRUNGCLASS', 'TP HUE',
        User('TRUNG', 'images/trung.jpg', 'trung@gmail.com'), false),
  ];

  var _listevent = BehaviorSubject<List<Event>>();
  Stream<List<Event>> get listevent => _listevent.stream;
  DemoRxDartBloC() {
    _listevent.add(event);
  }

  var _isGoingButton = BehaviorSubject<bool>.seeded(true);
  Stream<bool> get isGoingButton => _isGoingButton.stream;

  var _isFavoriteButton = BehaviorSubject<bool>.seeded(true);
  Stream<bool> get isFavoriteButton => _isFavoriteButton.stream;

  var _isShareButton = BehaviorSubject<bool>.seeded(true);
  Stream<bool> get isShareButton => _isShareButton.stream;

  var _isShowMore = BehaviorSubject<bool>.seeded(false);
  Stream<bool> get isShowMore => _isShowMore.stream;

  void setLike(int i) {
    event[i].isLike = !event[i].isLike;
    _listevent.add(event);
  }

  void setGoingButton() {
    _isGoingButton.add(!_isGoingButton.value);
  }

  void setFavoriteButton() {
    _isFavoriteButton.add(!_isFavoriteButton.value);
  }

  void setShareButton() {
    _isShareButton.add(!_isShareButton.value);
  }

  void setShowMore() {
    _isShowMore.add(!_isShowMore.value);
  }

  void depose() {
    _isGoingButton.close();
    _isFavoriteButton.close();
    _isShareButton.close();
    _isShowMore.close();
    _listevent.close();
  }
}
