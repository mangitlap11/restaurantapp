import 'package:flutter/material.dart';

class AppBar2Screen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Container(
          height: 292,
          width: MediaQuery.of(context).size.width,
          decoration: BoxDecoration(
            image: DecorationImage(
              image: AssetImage('images/image.png'),
              fit: BoxFit.cover,
            ),
            borderRadius: BorderRadius.circular(10),
          ),
          child: Stack(
            children: [
              Positioned(
                right: 5,
                left: 5,
                top: -170,
                child: SizedBox(
                  width: MediaQuery.of(context).size.width,
                  child: TextButton(
                    child: Image.asset(
                      'images/top.png',
                      width: 450,
                      height: 450,
                    ),
                    onPressed: () {
                      Navigator.pop(context);
                    },
                  ),
                ),
              ),
              Positioned(
                bottom: 10,
                right: 290,
                left: 5,
                child: TextButton(
                    onPressed: () {},
                    child: Image.asset(
                      'images/price.png',
                    )),
              ),
            ],
          ),
        ),
      ],
    );
  }
}
