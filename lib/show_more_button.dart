import 'package:btapp/view_detail/demo_rxdart.dart';
import 'package:flutter/material.dart';

class ShowMoreScreen extends StatefulWidget {
  @override
  _ShowMoreScreenState createState() => _ShowMoreScreenState();
}

class _ShowMoreScreenState extends State<ShowMoreScreen> {
  DemoRxDartBloC _demoRxDartBloC = DemoRxDartBloC();

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        StreamBuilder<Object>(
            stream: _demoRxDartBloC.isShowMore,
            initialData: false,
            builder: (context, snapshot) {
              return Container(
                child: Text(
                  'Lorem ipsum dolor sit amet, legimus salutandi usu ad, velit munere adversarium vis ei, an legere epicurei sadipscing vis. Ex vim soneteverti Lorem ipsum dolor sit amet, legimus salutandi usu ad, velit munere adversarium vis ei, an legere epicurei sadipscing vis. Ex vim soneteverti ',
                  overflow: TextOverflow.ellipsis,
                  style: TextStyle(fontSize: 16),
                  maxLines: snapshot.data ? 50 : 3,
                ),
              );
            }),
        Padding(
          padding: const EdgeInsets.only(top: 16.0, bottom: 32),
          child: Row(
            children: [
              InkWell(
                  child: Text(
                    'Show more',
                    style: TextStyle(color: Colors.blue, fontSize: 16),
                  ),
                  onTap: () {
                    _demoRxDartBloC.setShowMore();
                  }),
            ],
          ),
        ),
      ],
    );
  }
}
